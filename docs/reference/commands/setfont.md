---
title: "selfont"
weight: 9
weight: 9
description: Set the font used for various objects
categories:
 - Commands
 - Interface
tags:
 - images
 - font
 - drawing
 - labels 
---

>A {Console, Picture_labels} 

>B Brings up the dialog to choose a font for the Console or Labels, which end up on the picture. Use the built in function 'choosefont([olex2])' to choose a system. Alternatively, a specially prepared/portable font can be used to specify the font.
