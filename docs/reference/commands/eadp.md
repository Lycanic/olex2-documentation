---
title: "eadp"
weight: 9
description: Equal ADP constraint
categories:
 - Commands
tags:
 - refinement
 - constraints
 - ADP
---

>A atoms

>B Makes the ADP of the specified atoms equivalent.
