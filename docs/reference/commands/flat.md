---
title: "flat"
weight: 9
description: Restrain all selected atoms to lie in a plane
categories:
 - Commands
 - Model Building
tags:
 - refinement
 - restraints
---
>A [atoms] [esd=0.1]

>B Restrains given fragment to be flat (can be used on the grown structure) within given esd.
