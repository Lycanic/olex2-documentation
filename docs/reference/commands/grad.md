---
title: "grad"
weight: 9
description: Change the colour of the gradient background
categories:
 - Commands
tags:
 - graphics
 - display
---
>A [C1 C2 C3 C4] [-p]

>B Choose the colour of the four corners of the graduated background. 

>C
 * **-p**: a file name for the picture to be placed in the background
