---
title: "MSDSView"
weight: 9
description: Displays the Mean Square Displacement options.
categories:
 - Commands
tags:
 - analysis
 - symmetry
 - anharmonic
---

>A [atoms] [-a] [-q] [-q] [-t]

>B Displays MSDS options

>C
 * **-a**: anharmonicity display [all], anh, none
 * **-q**: quality [5], max is set to 7
 * **-s**: scale [1]
 * **-t**: type [rmsd], msd
