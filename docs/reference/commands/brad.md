---
title: "brad"
weight: 9
description: Adjusts the bond radii in the display
categories:
 - Commands
tags:
 - display
---

>A *r* [hbonds]

>B Adjusts the bond radii in the display. If *hbonds* is provided as the second argument, the given radius *r* is applied to all hydrogen bonds. This operates on all or selected bonds.
