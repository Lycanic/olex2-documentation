---
title: "mode fix u"
weight: 9
description: Fixes Uiso or ADP for subsequently clicked atoms
categories:
 - Commands
categories:
 - Model Building
tags:
 - refinement
 - disorder
---

>B Fixes Uiso or ADP for subsequently clicked atoms.

