---
title: "scaleN"
weight: 9
date: 3/11/2020
description: Extends the normal of one plane to touch another plane
categories:
 - Commands
tags:
 - analysis
---

>D Generate two planes (of two phenyl rings, for example). Then select both of these planes and type `scaleN`. This will generate a line perpendicular to the first plane (the [*normal*](/olex2/docs/glossary/#normal)). The length of the line is scaled so it will end where it intersects the second plane.

/olex2/docs/glossary/#normal

[*Get in Touch*](/about/#contact))