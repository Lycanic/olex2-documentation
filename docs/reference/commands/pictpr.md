---
title: "picta"
weight: 9
description: Creates a POVRAY file of the current view
categories:
 - Commands
tags:
 - image
 - drawing
 - POVRAY
---

>A filename

>B Creates PovRay file for current view
