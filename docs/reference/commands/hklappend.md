---
title: "hklappend"
weight: 9
description: Append reflections to the hkl file
categories:
 - Commands
 - Interface
tags:
 - refinement
 - .hkl
---

>A -h=h1;h2;.. -k=k1;k2.. -l=l1;l2..

>B Acts in the opposite way to `CODE excludehkl`.
