---
title: "OFileSwap"
weight: 9
description: Select the active file when overlay is on
categories:
 - Commands
tags:
 - overlay
---

>B Sets the current file to which all commands are applied
