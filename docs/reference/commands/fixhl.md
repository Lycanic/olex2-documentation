---
title: "fixhl"
weight: 9
description: Hydrogen atom names follow the parent atom
categories:
 - Commands
tags:
 - view
 - display
 - analysis
---

>B Updates H-atom labels according to the labels of the bearing atoms
