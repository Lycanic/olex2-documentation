---
title: "HKL file Operations"
linkTitle: "HKL file Operations"
weight: 9
description: "What Olex2 can do with the hkl file"
---
 
>B Prints detailed information about reflections used in the refinement.



## omit

>A h k l

>B Inserts `CODE OMIT h k l` instruction in the ins file. Use `CODE delIns omit` to remove all the OMITs from the INS file header.

>A `CODE omit val`

>B Inserts `OMIT h k l` for all reflections with $|{F_{o}}^2 - {F_{c}}^2| > val$

>A omit s 2theta

>B Inserts `CODE OMIT s 2theta` instruction in the ins file


## hkledit

>A [h k l]

>B Brings up a dialogue, where "bad" reflections from the Shelx lst file and all its constituent symmetry equivalents can be inspected and flagged to be excluded from the refinement.
In contrast to the `CODE OMIT h k l` instruction, which  excludes the reflection and all it equivalents, this dialogue allows the exclusion of those equivalents that are actually outliers.
If a particular reflection is specified, this particular reflection and all its constituent equivalents can be viewed.


## hklexclude

>A [-h=h1;h2;.. -k=k1;k2.. -l=l1;l2.. [-c]

>B This function provides a mechanism to reversibly exclude some reflections from refinement (these reflections will be moved to the end of the .hkl file so they appear after the `0 0 0` reflection).

>C
 * **-c**: option controls how the given indices are treated. If no **-c** option is provided, then any reflection having any of the given *h*, *k* or *l* indices will be excluded; otherwise only reflections with indices within provided *h*, *k* and *l* will be excluded.


## hklappend

>A -h=h1;h2;.. -k=k1;k2.. -l=l1;l2..

>B Acts in the opposite way to `CODE excludehkl`.


## hklview

>A [-h=h1;h2;.. -k=k1;k2.. -l=l1;l2.. [-c]

>B Shows the reflection currently used in the refinement (use CTRL+T a few times to centre on the reflection view).

  