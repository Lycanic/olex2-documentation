---
title: "Fixed and Refined Parameters"
linkTitle: "Fixed and Refined Parameters"
weight: 4
description: "Fix and free parameters: occupancy, xyx, uiso"
---
 
>A {occu, xyz, Uiso} [atoms]

>B Fixes the specified refinement parameter, i.e. these parameters will not be refined in subsequent refinement cycles.

>C
 * **-occu**: will fix the occupancy 
 * **-xyz**: will fix the xyz coordinates
 * **-Uiso**: will fix the whole ADP

>D `CODE fix occu 0.5`: will set and fix the occupancy of the current selection to 0.5
`CODE fix xyz`: will fix the x, y and z co-ordinates of the currently selected atoms (i.e. not refine them).


## free

>A {occu, xyz, Uiso} [atoms]

>B The opposite of FIX - makes the specified parameters for the given atoms refineable. Freeing the occupancy is also available from the context menu.


## mode fixu

>B Fixes Uiso or ADP for subsequently clicked atoms.



## mode fixxyx

>B Fixes the coordinates for subsequently clicked atoms.


## mode occu

>A occupancy_to_set

>B Sets atom occupancy to the provided value for subsequently clicked atoms.
  