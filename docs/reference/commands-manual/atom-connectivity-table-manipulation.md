---
title: "Atom Connectivity"
linkTitle: "Atom Connectivity"
weight: 5
description: "Changing the way atoms are connected to each other"
---


How does Olex2 'decide' whether there is a bond between atoms or not? For most structures this isn't an issue, and the connectivity displayed in Olex2 wll be as you expect it to be. But sometimes this isn't so easy and it is good to try and understand how Olex2 works out where there should be bonds.

Each atom has a pre-defined (but customisable!) bonding radius ($r$).

So bonds are rendered for two atoms ($A$ and $B$) at distance $d$ if

$d - r_{A} - r_{B}$ &lt; delta

Angles are not taken into account when working out these bonds.

After selecting an atom, you can do the following
- `CODE info`: the radii will be printed
- `CODE delta`: show the value of delta used for this atom
- `CODE delta 0.6`: update the value of delta to '0.6'
- `CODE CONN 8`: set the maximum number of bonds for this atom to '8' (default: 12)

When using the CONN command, the longest bonds are excluded first. You can also set this connectivity for groups of atoms, for example:

For all metals in current structure:
- `CODE conn $M 6`:  sets max number of bonds for all metals *in this structure*
- `CODE conn $M 1.7` - sets bonding radius for all metals to 1.7
- `CODE conn $M 1.7 6` - sets both

You can override the default bonding radii 'globally' by loading them from a file:

- `CODE load radii bonding` - you can then choose a file containing lines with the element followed by the radius for that element.

These radii will be used for the current  Olex2 session and you will have to reload the file after a restart again.

You can also adjust the connectivity table by adding custom *bonds*

- select two atoms, then type `CODE addbond`
- select bond(s) and type `CODE delbond` to delete existing bonds.


{{% expanded_tag tag="connectivity-table" %}}
