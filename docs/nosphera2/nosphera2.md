---
title: "NoSpherA2"
linkTitle: "Intro"
date: 09-11-2020
weight: 7
description: "Non-Spherical Atomic Form Factors in Olex2"
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - Quantum Crystallography
 - HAR

---

{{< webp image="/images/nosphera2_enable.jpg"  width="480" alt="The GUI of NoSpherA2 appears when olex2.refine is selected">}}

When olex2.refine is selected as the refinement software and NoSpherA2 is ticked, **non-spherical form factors**  will be used in the refinement of the structure.

This requires a table of scattering or form factors for each atom type to be present in the shape of a *.tsc* file to be present. By default the "**Update Table**" box is enabled. This will let you select which software to use for calculation of a *.tsc* file.

>OLEX2 NoSpherA2 is a new procedure inside Olex2, which requires intensive computing resources. Testing was performed using ORCA, pySCF and Tonto as .tsc sources. It replaces the previously available HARt, which was based on Tonto only. Tonto is not recommended for elements heavier than period 3 due to inaccuracies during the wavefunction calculation.


{{< youtube id="qALACf4DOnw" >}}


### Hirshfeld Atom Refinement (HAR)

For **HAR** there are three steps in this procedure:

1. The molecular wavefunction is obtained for your input model using Quantum Mechanical calculations using:
     - **TONTO** (shipped)
     - **ORCA** (Versions 4.1.0 and up, obtainable from URL[https://orcaforum.kofo.mpg.de/index.php,LINK])
     - **pySCF** (Follow the installation guide by selecting "Get pySCF" as *.tsc* source)
     - **Gaussian** of various versions (implemented but not maintained)
2. The atomic non-spherical form factors are extracted from the molecular wavefunction by Hirshfeld partitioning of the atoms in the molecule through NoSpherA2.
3. olex2.refine now uses these non-spherical form factors for the next refinement least-squares cycles. All normal commands for your refinement can be used, including restraints and contraints.

At this point, a new model will be obtained, and this **will** require the re-calculation of the molecular wavefunction -- and the above three steps need to be repeated until there is no more change and the model is completely settled. This process can be autmoatized by using the **HAR** switch. Otherwise this procedure is called a *rigid body fit*.

If you want to use more CPUs make sure the proper MPI Installation is found on your computer. For Windows user install MS-MPI of at least Version 10, for Linux Users openMPI version 4 or up is required. MacOS users please refer to homebrew and install apropiate version of openMPI. MacOS executables are however, due to the developer unfriendlyness of the OS not maintained anymore and the use on any Mac for NoSpherA2 is discouraged in favour of a real Computer. The mpiexec(.exe) is needed to be found in the PATH, either through "Settings" in the Olex2 HOME tab or Environment Variables.

A recommended strategy for efficient refinements using ORCA or pySCF is:

  - PBE/DZP and Normal Grids
  - PBE/TZP and Normal Grids
  - PBE/TZP and High Grids

In case you want to make sure it is the best possible fit you can try finishing up with meta or hybrid functionals, but in the benchmarks it did not change results significantly. In case of all atoms lighter than Kr it is best to use **def2**- familiy of basis sets. In case of any heavy element or significant relativistic effects use **x2c-** or **jorge-** basis sets with relativistics on.

## Literature

- Jayatilaka & Dittrich., Acta Cryst. 2008, A64, 383
&nbsp; URL[http://scripts.iucr.org/cgi-bin/paper?s0108767308005709,PAPER]
- Capelli et al., IUCrJ. 2014, 1, 361
&nbsp; URL[http://journals.iucr.org/m/issues/2014/05/00/fc5002/index.html,PAPER]

>CRYST If you use NoSpherA2 you agree to cite the following literature in your main manuscript.

- Kleemiss et al., Chem. Sci., DOI: 10.1039/D0SC05526C
&nbsp; URL[https://pubs.rsc.org/en/content/articlepdf/2021/sc/d0sc05526c,PAPER]

### Update Table

After ticking the 'NoSpherA2' box, the option **Update Table** will appear. Depending on your selection different techniques can be used.

For **HAR** the default method to calculate the wavefunction is **Tonto**, since it can be shipped with Olex2. However, this software has significant flaws and speed limitation, which is why the use of **ORCA** or **pySCF** is highly recommended.

If you update the table all present *.wfn* and *.tsc* files will be backed up in the olex2/NoSpherA2_backup folder with your structure, if you ever want to go back.

The most benchmarked and recommended software is **ORCA**. **pySCF** as the free and open source alternative works almost as good. Sometimes a little fine tuning might be required. Please contact us if you encounter such a case. There is also an option to use **Gaussian** [HIGHLY UNTESTED]. A present **.wfn** file can also be used but **must** coincide with the geometry currtly used in Olex2. These options will appear, depending on whether they are properly installed and Olex2 knows about them -> Settings and PATH). Once a program has been chosen, please also adjust the Rfinement Settings according to your needs. This is the case if you do not want to use the bare minimal settings.

If Update Table is deactivated a dropdown of all *.tsc* files found in the current folder is given to be used for refinement, without updating the files.