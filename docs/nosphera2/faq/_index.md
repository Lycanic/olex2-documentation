---
title: "Frequently Asked Questions"
linkTitle: "FAQ"
date: 09-11-2020
weight: 12
description: "Selected questions that occurred."
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - Quantum Crystallography
 - HAR
 - FAQ

---

This is a non-complete list of questions and topics that one might encounter when NoSpherA2 is used. If you are missing any information please contact us with your question!
