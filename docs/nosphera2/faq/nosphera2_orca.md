---
title: "ORCA"
linkTitle: "ORCA"
date: 09-11-2020
weight: 19
description: "How do I install ORCA?"
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - Quantum Crystallography
 - HAR
 - FAQ
 - Basis Sets
---

Since ORCA is third-party software, you will need to register and download ORCA independently. It can be obtained from the ORCA Forum: https://orcaforum.kofo.mpg.de

If you use Windows you can install the self-extracting installer and most things should be finished. You will need to install Microsoft MPI Version 10, which can be found at https://docs.microsoft.com/en-us/message-passing-interface/microsoft-mpi. If you use Unix based systems like Ubuntu/CentOS/RedHat/MacOS you can download the corresponding version of ORCA, which will be unpacked into a folder of your choice. Make sure you remember where to tell Oelx2 later. You will also need to install **OpenMPI**. In the case of Linux distributions, it is recommended to install it from sources, in the case of MacOS it can be installed through homebrew.

You need to make sure that the PATH to ORCA (and in the case of UNIX systems to OpenMPI's **mpiexec**) is set correctly in the Olex2 settings under "HOME". After a restart of Olex2 you should be able to use ORCA using Multiple CPUs in NoSpherA2.