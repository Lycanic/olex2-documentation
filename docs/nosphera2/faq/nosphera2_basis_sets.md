---
title: "Basis Set"
linkTitle: "Basis Sets"
date: 09-11-2020
weight: 20
description: "When to use which Basis Set"
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - Quantum Crystallography
 - HAR
 - FAQ
 - Basis Sets
---

There is no final answer to this question. It is a matter of the level of sophistication you want to have in your model and therefore in your results. If you employ a less sophisticated basis set you will have a result more quickly, but at lower accuracy. Generally speaking the selection of basis set can be separated in 3 different steps:

1. ***Do I want high precision or am I still testing/changing things***? If High precision is required go for TZVP or 6-311G basis sets. Else start with something cheaper, like def2-SVP or jorge-DZP
2. ***Do I need/want relativistic calculations?*** Yes: jorrge-X-DKH or x2c-familiy basis sets. Else the jorge basis ets without -DKH suffix and all other basis sets work. Keep in mind that only up to Kr is defined in the non-jorge basis sets.
3. **Is my system anionic?** If you grow things you might have a resulting calculation which is negatively charged. This system will msot likely require diffuse basis functions (unless you can compensate using a solvation model). In this case e.g. def2-TZVPD and def2-TZVPPD and 6-311++G basis sets have the diffuse functions you are looking for.