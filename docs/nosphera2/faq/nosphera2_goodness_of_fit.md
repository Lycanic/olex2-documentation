---
title: "Goodness of Fit"
linkTitle: "GooF"
date: 09-11-2020
weight: 13
description: "What does the Goodness of Fit tell me in NoSpherA2? Why is it sometimes worse than in the IAM?"
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - Quantum Crystallography
 - HAR
 - FAQ
 - Basis Sets
---

Generally speaking the Goodness of Fit (GooF), when using SHELX type weighting schemes is a bit fuzzy in NoSpherA2. We have not yet fully understood how it behaves.

If you run refinements using the weighting scheme you will observe a significant change in the coefficients **a** and **b**, which will dramatically drop in comparison to the spherical model. This is because the atomic shape is modelled more accurately and therefore less "damage repair" needs to be done by the weighting scheme.

This being said if you run the refinement with weighting schemes being 0 0, you can directly use the GooF as a measure for the quality of your model.

- If it is **close to 1** you are statistically within 1 sigma of your measured structure factor of your model. 
- Values significantly **below 1** hint towards overestimated sigmas of your measured data.
- If it is significantly **above 1** (which will msot likely be the case in routine measurements) there is some un-modelled information in the data.

Try to look at residual density maps and look for systematic errors like improper absorption correction around heavier elements, mismatching dispersion correction or un-modelled disorder.

