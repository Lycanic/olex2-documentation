---
title: "Hydrogen Atom Positions and ADPs"
linkTitle: "Hydrogen"
date: 09-11-2020
weight: 12
description: "Hydrogen Atom Positions and Atomic Discplacement Parameters (ADPs) can not be determined from X-ray diffraction, are they calculated?"
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - Quantum Crystallography
 - HAR
 - FAQ
---
The position of hydrogen atom can not be accurately determined from diffraction experiments when the *spherical model* -- the so called IAM -- is used, as it is the case in most routine structure refinements. 

If *non-spherical* atoms are used, which, in the case of **Hirshfeld Atom Refinement** (see Capelli et al. IUCrJ, 2014, 1, 361-379 URL[https://journals.iucr.org/m/issues/2014/05/00/fc5002/index.html,PAPER]), are tailor-made for the very specific structure at hand, the deformation of hydrogen atoms can be described and included in the model and ultimately it becomes possible to obtain accurate and precise atom positions and also decent ADPs. 

This was validated against Neutron Diffraction data for example in Woinska et al. **ChemPhysChem**, 2017, **18**, 3334-3351. URL[https://chemistry-europe.onlinelibrary.wiley.com/doi/abs/10.1002/cphc.201700810,PAPER].

This also holds for Cu-radiation data with relatively limited resolution available, since the information about hydrogen atoms is contained in the low-resolution data (compare the form-factor of elements with respect to resolution). This allows accurate determination of hydrogen atoms also from Cu-radiation experiments.

In the vicinity of very heavy elements this situation is still not clear, but fourth and fifth period elements seem to work fine in the test cases investigated.
