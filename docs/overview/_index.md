---
title: "Overview"
linkTitle: "Overview"
type: docs
weight: 1
description: "What is Olex2?"

---

Olex2 is an intuitive and easy to use crystallographic program deidcated especially to solve crystal structures of small molecules.

It also provides powerful tools for creating pictures and automatically created reports of your structures!

An extensive and intuitive GUI as well as a powerful command line for the usual ShelX commands
Olex2 gets updated regularly and is always in close contact to its users!

### Features

Olex2 provides all the functionality that is required to work with small-molecule crystal structures.

- Structure Solution
- Structure Refinement
- Structure Analysis
- Image Generation
- Report Generation
- Automation

Take a look at the very basic features:

{{< youtube id="8QvmKSnJFPs" >}}


### Why Olex2? 

* **Why Olex2?**: If the terms **ShelX**, **PLATON**, **WINGX** and **ShelXle** mean anything to you, Olex2 is for you.

* **What does Olex2 excell at?**: Olex2 is fast-working, intuitive but also versatile, clean GUI for the treatment of small molecule X-ray data with many features.

* **What does Olex2 NOT do?**: Olex2 is best for small molecule data 


### How can I get started?

Simply download Olex2 here:

* [Download](/olex2/docs/getting-started/installing-olex2/)

and get into the software with our built-in example structures and explanations:

* [Tutorials](/olex2/docs/tutorials/)

A documentation, further examples and discussion about Olex2 related topics can all be found on this Website.
