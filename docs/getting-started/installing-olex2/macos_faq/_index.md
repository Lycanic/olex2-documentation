---
title: "Installing Olex2 on MacOS: FAQ"
linkTitle: "MacOS FAQ"
date: 2021-03-01
weight: 10
description: Installing Olex2 on the MacOS
categories:
 - Installation
tags:
 - MacOS
 - ShelX
 - ShelXL
---

Installing Olex2 and ShelX on the MacOS system can be tricky, and what exactly needs to be done to make things work depends on the system you are using. Here are some questions we had in the past, in no particular order.
