---
title: "Making ShelX work with Olex2 on the Mac"
linkTitle: "ShelX"
date: 2021-03-01
weight: 10
description: How do I make ShelXS/T and ShelXL work with Olex2 on the Mac?
categories:
 - Installation
tags:
 - MacOS
 - ShelX
 - ShelXL
---


You may find that Olex2 does not seem to recognize ShelXS, ShelXT and ShelXL. There are two reasons for this: ShelX has not been installed properly, and/or ShelX is not on the system path. Olex2 can only find programs if they are either on the system path, or the path has been set in Home > Settings in Olex2.

## For Mac 10.15 or higher

Self-installed programs (like ShelXL) can ***not be installed in*** folders inside __Desktop__ or __Documents__. 

You can also ***not have your structure data*** inside these two folders, or any sub-folder of them. The Mac security features will prevent things from working inside of these folders.

The workaround is to create a new folder under the login folder of the Mac, so **not** inside __Documents__ and **not** inside __Desktop__.  Then work from there.

For ease of use, an alias to this new working folder can *probably* be put
on the __Desktop__ or in __Documents__.

## Defining the PATH in Olex2

When you define a **PATH** in the Olex2 Settings (Home > Olex2), and need to have more than one path, then  the separator is a **colon** on the Mac, not a semi-colon.

For example:
```
/users/Fred/Applications/bin:/usr/local/bin
```



## Make sure things are executable

Is something doesn't run on MacOS -- i.e. if Olex2 shows an option but it fails to execute, then you need to make sure that the program is executable, for example

```
chmod +x shelxs
```