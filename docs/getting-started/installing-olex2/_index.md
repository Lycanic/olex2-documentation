---
title: "Installation Notes"
linkTitle: "Installation"
date: 2020-06-2020
weight: 10
robotsdisallow: true
type: docs
categories:
 - Installation
 
tags:
 - Windows
 - Linux
 - MacOS
 
---

Installing Olex2 should be a simple procedure.

- Olex2 runs on all common computer platforms
- There is no complicated installation procedure
- Nothing needs to be compiled
- Everything you need to perform small-molecule structure solution and refinements is included

##  Windows

The Olex2 installer for windows is a small program which offers various installation options. You can select which **[version](/olex2/docs/getting-started/versions)** you would like to install.

{{< webp image="/images/windows_installer.png" alt="The windows installer" width="70%" >}}

You can start with the **default settings** and explore different version when you need them. For example, you may want to run the _alpha_ version of Olex2 as well as the _Release_ version. In any case, each major version of Olex2 will automatically update within its own series.

### Windows Installer

{{< download_link id="26" text="Download Olex2 Installer for Windows" logo="win_logo.svg" >}}


### Windows ZIP files

Alternatively, please unzip these ZIP files to any directory on your system.

{{< download_link id="43" text="Olex2-1.5 zip archive for Windows 64 Bit" logo="win_logo.svg" logo_width="18px">}}
{{< download_link id="42" text="Olex2-1.5 beta zip archive for Windows 64 Bit" logo="win_logo.svg" logo_width="18px">}}
{{< download_link id="39" text="Olex2-1.5 alpha zip archive for Windows 64 Bit" logo="win_logo.svg" logo_width="18px">}}

#### Archive

{{< download_link id="40" text="Olex2-1.3 zip archive for Windows 64 Bit" logo="win_logo.svg" logo_width="18px">}}

<br>

##  MAC OS

For Mac OS we provide one DMG installer of the current version Olex2-1.3, which will automatically update. There is also the option of downloading the relevant zip files and installing Olex2 manually.

### MAC OS Installer

{{< download_link id="44" text="Olex2-1.5 installer file for Mac-OS 64 Bit" logo="MacOS_logo.svg" >}}

<br>

### MAC OS ZIP files

{{< download_link id="35" text="Olex2-1.5 zip archive for Mac-OS 64 Bit" logo="MacOS_logo.svg" logo_width="18px">}}
{{< download_link id="45" text="Olex2-1.5 beta zip archive for Mac-OS 64 Bit" logo="MacOS_logo.svg" logo_width="18px">}}
{{< download_link id="38" text="Olex2-1.5 alpha zip archive for Mac-OS 64 Bit" logo="MacOS_logo.svg" logo_width="18px">}}

<br>

### MAC Archive


{{< download_link id="27" text="Olex2-1.3 installer file for Mac-OS 64 Bit" logo="MacOS_logo.svg"logo_width="18px">}}
{{< download_link id="28" text="Olex2-1.3 zip file for Mac-OS 64 Bit" logo="MacOS_logo.svg" logo_width="18px">}}



<br>

##  Linux

We do not provide installers for Linux -- and provide zip files instead.

{{< download_link id="47" text="Olex2-1.5 zip file for Linux 64 Bit" logo="linux_logo.svg" logo_width="18px">}}
{{< download_link id="46" text="Olex2-1.5 beta zip file for Linux 64 Bit" logo="linux_logo.svg" logo_width="18px">}}
{{< download_link id="37" text="Olex2-1.5 alpha zip file for Linux 64 Bit" logo="linux_logo.svg" logo_width="18px">}}


### Linux Archive

{{< download_link id="30" text="Olex2-1.3 zip file for Linux 64 Bit" logo="linux_logo.svg" >}}


