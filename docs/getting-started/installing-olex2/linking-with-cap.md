---
title: "Linking Olex2 with CrysAlisPro (Rigaku Oxford Diffraction)"
linkTitle: "Linking with CAP"
date: 2020-06-2020
weight: 10
description: Setting up which version of Olex2 to start from from CrysAlisPro (ROD)

categories:
 - Installation

tags:
 - Windows
 - Rigaku Oxford Diffraction
 - CrysAlisPro
---

![Opening the command shell | Options RED | Programs | Browse](../cap_olex2.jpg)

Olex2 can be started straight from the Rigaku Diffractometer and data reduction software CrysAlisPro.

You can set _which_ version of Olex2 is linked by opening the CrysAlisPro settings. 



