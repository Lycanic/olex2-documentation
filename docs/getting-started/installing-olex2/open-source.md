---
title: "Olex2 is Open Source"
linkTitle: "Open Source"
date: 2020-12-05
weight: 10
description: Project Components
categories:
 - Installation
tags:
 - licence
 - cctbx
 - smtbx
---

Olex2 is an open source project and you are invited to inspect, check and contribute to its code. There are a number of components to Olex2.

## Components of Olex2
Olex2 is modular. The basic operations are well separated from the graphical user interface (GUI) and these two main parts are maintained separately.

### The Olex2 Executable
This is the underlying engine of Olex2 that does all the work. This part is written in C++ and is maintained by us. The Olex2 executable provides an embedded [Python](https://www.python.org) interpreter and a container for the portable HTML-based GUI developed using [wxWidgets](https://www.wxwidgets.org).

### The Olex2 GUI
The GUI is written in a combination of Python and (extended) HTML. The GUI panel that contains all tools and visual feedback is effectively a website. This GUI code is maintained by us and has had contributions from many Olex2 users.

### The cctbx
This is another component used by the Olex2 application - the **C**omputational **C**rystallography **T**ool**b**o**x** ([cctbx](https://cci.lbl.gov/cctbx_docs/index.html)). This is a huge project, maintained by Lawrence Berkeley National Laboratory, Berkeley. Olex2's structure refining (_olex2.refine_) and charge flipping structure solution (_olex2.solve_) have roots in the _smtbx_, the **S**mall **M**olecule **T**ool**b**o**x**. The cctbx also forms the basis of the macromolecular [PHENIX](https://www.phenix-online.org/documentation/index.html) project.


## Licence
Olex2 is released under the [BSD](https://en.wikipedia.org/wiki/BSD_licenses) standard licence. Here is the full text of the Olex2 licence.


<div class='box'>
<code>
Olex2 

Licence for use and distribution 

If you are using Olex2, you agree to cite the program in your publications:

Dolomanov, O.V.; Bourhis, L.J.; Gildea, R.J.; Howard, J.A.K.; Puschmann, H., OLEX2: A complete structure solution, refinement and analysis program (2009). J. Appl. Cryst., 42, 339-341.

Olex2 was developed under the EPSRC-funded open source project 'Crystallographic Software for the Future' (Grant No EP/C536274/1). This grant was held by Durham University from 2005-2010.

All continuing development is undertaken by OlexSys Ltd (www.olexsys.org)

However, we keep the exclusive right to produce executables under the name OLEX2. This is what is referred to in this licence agreement. OLEX2 is distributed as freeware.

Conditions of this license: 

1. All copyrights to the OLEX2 executables are exclusively owned by the authors - Oleg Dolomanov & Horst Puschmann 

2. THE OLEX2 EXECUTABLES ARE DISTRIBUTED "AS IS". NO WARRANTY OF ANY KIND IS EXPRESSED OR IMPLIED. YOU USE IT AT YOUR OWN RISK. THE AUTHORS WILL NOT BE LIABLE FOR DATA LOSS, DAMAGES, LOSS OF PROFITS OR ANY OTHER KIND OF LOSS WHILE USING OR MISUSING THIS SOFTWARE. 

5. Installing and using the OLEX2 executables signifies acceptance of these terms and conditions of the license. 

6. If you disagree with any of the terms of this license you must cease using the product and remove all components of the OLEX2 program from your storage devices. 

7. Anyone may distribute copies of the OLEX2 executables, completely unaltered, without further permissions. A nominal fee for providing copies of OLEX2 program to individuals or organizations can be charged. A charge not exceeding £5.00 is suggested. 

8. The maintenance of the the OLEX2 executables is exclusively owned by the authors – Oleg Dolomanov & Horst Puschmann. 
  
9. There may exist add-ons as part of the OLEX2 distribution which are not covered by this license.

Thank you for using OLEX2. 
 
Oleg Dolomanov & Horst Puschmann 
</code>
</div class='box'>

`
