---
title: "Mouse Tracking"
linkTitle: "Mouse"
weight: 7
description: "How to use the mouse in Olex2"
---
 

Once you get used to using Olex2, the mouse actions will become second nature to you. Depending on what software you have used before, you will find the mouse actions in Olex2 more or less intuitive.

|Command					|	 		| Description |
|-----------------------  	|:---------:| -------------------------------------|
|**HOLD LEFT MOUSE**				| {{<svg "left-mouse.svg" "10px">}}	| Rotates the model
|**HOLD RIGHT MOUSE**		| {{<svg "right-mouse.svg" "20px">}}	| Zooms the view (or zoom-able objects)
|**ALT + LEFT CLICK**		| {{<svg "left-mouse.svg" "20px">}}	| Zooms the view (or zoom-able objects)
|**CTRL + LEFT CLICK**		| {{<svg "left-mouse.svg" "20px">}}	| Rotate around the (Z) axis perpendicular to the screen
|**HOLD LEFT + HOLD RIGHT**	| {{<svg "both-mouse.svg" "20px">}}	| Moves the molecule laterally
|**CTRL + SHIFT + HOLD LEFT**	| {{<svg "left-mouse.svg" "20px">}}	| Moves the molecule laterally
|**SHIFT + HOLD LEFT**		| {{<svg "left-mouse.svg" "20px">}}	| Draw box to select atoms
|**LEFT CLICK on object**	| {{<svg "left-mouse.svg" "20px">}}	| Selects the object (atom, bond, plane)
|**DOUBLE CLICK on an atom**| {{<svg "left-mouse.svg" "20px">}}	| Selects the molecule
|**RIGHT CLICK background**	| {{<svg "right-mouse.svg" "20px">}}	| Opens the display settings menu
|**RIGHT CLICK on object**	| {{<svg "right-mouse.svg" "20px">}}	| Open the object property (context) menu
---------------------  ------------------------------------------------------
  