---
title: "Getting Around Olex2"
linkTitle: "Getting Around"
weight: 30
description: "A guide to the Olex2 interface."
---
 

Olex2 provides a Graphic User Interface (GUI) through which many common crystallographic tasks can be completed.
  