---
title: "Deep Olex2 Options"
linkTitle: "Options"
date: 2021-05-11
weight: 20
categories:
 - Installation
 
tags:
 - customisation
 - OpenGL
 - default
---

There are some 'deep options' that set the behaviour of Olex2 at some deep level. These options affect various different aspects of the overall Olex2 operation, including some important settings regarding the structure model. Please take care when changing these values!

https://github.com/pcxod/olex2/blob/master/options.txt

Type the word `options` into Olex2, and an editor will open. This editor will only contain those options where the default value has been changed.


## User interface options

| Variable | Default | Desription |
| -------- |:-------:| ---------- |
| mouse_invert_zoom  | False | specifies if the mouse zooming operation to be inverted |
| mouse_click_threshold  | 2 | specifiec the rectangluar area size where the mouse click is recognised |
| tooltip_occu_chem  | True | tooltip shows chemical occupancy vs crystallographic one |
| q_peak_min_alpha  |0 | minimum value for the Q-peak transparence (0-0.75) |
| model.center_on_reload  |true | re-centres the model on the file re-read (like after the refinement) |
| model.center_on_update  |true | re-centres the model when it is updated (like atoms split) |

## Olex2 behaviour options

| Variable | Default | Desription |
| -------- |:-------:| ---------- |
| path  | | - extensions to the system PATH (prepended) |
| cif.use_md5  |False | embedd MD5 checksums for HKL/FAB/RES into the CIF |
| p4p_automate | False | specifies if the SG/SGE to be executed after loading the P4P/CRS file (used in reap) |
| absolute_hkl_path  |False | saves absolute path in the INS file |
| profile  |False | switches application profiling on/off |
| confirm_on_close | False | asks a confirmation question before closing the application |
| max_label_length  |4 | new Shelxl will support longer labels in the INS file |
| locale.ctype  | | default locale. Yo may need to use en_US.utf8 for Linux/Mac |

## Options affecting the structure model

| Variable | Default | Desription |
| -------- |:-------:| ---------- |
| aromatic_rings | False | specifies if the aromatic rings are to be automatically created |
| aromatic_rings_def  |  | aromatic ring definitions. Defaults: C5,C6,NC5,SC4,N2C3,NCNC2 |
| hbond_min_angle  | 120 | the minimal angle fo H-bonds to exist |
| preserve_invalid_ins  | False | if invalid instructios are preserved vs being deleted |
| preserve_restraint_defaults  | False | if set, the default restraint values/weights are shown in the ins file. This option has no effect when DEFS instruction is set. |
| preserve_fvars | | preserves the even if they are refered only once |
| safe_afix  |True | checks AFIXes are correct after atom naming, deleting and HAdd itself |
| interactions_from | H | sets a list of atoms for which to display short contacts |
| interactions_to  |  | sets a list of atoms for which to display short contacts. Default: N,O,F,Cl,S,Br,Se  |
| group_restraints  |False | groups restraints by involved atoms |
| use_hkl_cell  | True | if HKL file has CELL instruction - Olex2 overrdes current file CELL with the parameters |
| rename_parts  | True | disallow identical labels within different parts |

## OpenGL options (Display of the molecule)

| Variable | Default | Desription |
| -------- |:-------:| ---------- |
| gl_selection  | True | to use the OpenGl selection implementation. If False - a more limited but working more precise with some drivers implementation is used. It however does not allow the selection of objets which use textures (unless treated specifically) or colours.. |
| gl_multisample  | True | enables smoothing of the rendering, though reduces the prformace it  produces better quality picture |
| gl_stereo  | True | enables stereo buffers. Note that if this option is enabled and your graphics card does not support stereo buffers, gl_multisample option will be turmed off be default at first Olex2 run. However, on exit, Olex2 will check this and will disable this option so that multisampling will work |
