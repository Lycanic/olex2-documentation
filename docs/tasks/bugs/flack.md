---
title: "Flack Parameter"
weight: 600
description: "The Flack parameter is not being correctly calcualted by olex2.refine"

categories:
- bugs

tags:
- x-ray
- bugs
- refinement
- non-centrosymmetric
- statistics
---


# Flack Parameter from olex2.refine

In ShelXL, the Flack parameter after refining a non-centrosymmetric is *significantly* different from the one in olex2.refine. When using the same .hkl and .ins ShelxL in one of my examples ends up with 0.013, olex2.refine results in 0.2, which is not trustable.

# using NoSpherA2

If on uses NoSpherA2 this is of special interest, since it might be woth to investigate if refining the structure non-spherically actually can improve the anomalous dispersion signal.
