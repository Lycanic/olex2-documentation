---
title: "Neutron Structures"
weight: 500
description: "What goes wrong with neutron Structures"

categories:
- Crystallography

tags:
- neutron
- bugs
- refinement
---


# FMAP -2 not working

In ShelXL, the `FMAP -2` instruction will result in +ve *and* -ve peaks after refinement. This doesn't work in olex2.refine, and the instruction will be automatically changed to `FMAP 2`.

# NEUT instruction not understood

olex2.refine does not seem to understand the `NEUT` instruction. For ShelXL, the presence of this instruction will make it use the neutron SFAC information. For olex2.refine, it is necessary to include the SFAC lines explicitly with `GenDisp -n`. Not a big deal, but the refinement will behave differently in the two refinement programs when only `NEUT` is present. Once the neutron SFAC's are present, the refinement will proceed similarly (if not identically) in the two programs.

# EXTI in neutron refinements

The two values of a refined `EXTI` are very different when comparing olex.refine and ShelXL.

# Weighting Scheme and GooF for neutron refinement

The weighting schemes (and GooF values) are very different when comparing olex.refine and ShelXL