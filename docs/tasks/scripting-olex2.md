---
title: "Scripting Olex2"
linkTitle: "Scripting"
weight: 2
description: "There are two different levels on which Olex2 can be scripted"
categories:
 - Interface
tags:
 - scripting
 - programming
 - batch
 - macro
---
 

The functionality of Olex2 can be extended on two levels: though macros and through python scripts.


## Olex2 Macros
There is a simple macro language, where you can group common tasks (and also redefine commands!). The built-in macros will be extended (and replaced!) with those macros you will find in the file you will see when you type `CODE emf`.


## Python Scripts
You can also script Olex2 using Python. All functions of Olex2 can be accessed and highly complex functionality can be added.
  