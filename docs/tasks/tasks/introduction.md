---
title: "Restraints and Constraints"
linkTitle: "Restraints and Constraints"
weight: 
description: "How to use Restraints and Constraints"
categories:
 - Interface
tags:
 - refinement
 - restraint
 - constraint
---
 
Although the crystallographic workflow - solve, refine, check, prepare images, tables and reports - appears to be fairly straightforward, in reality things are often much more complicated. This reference section of the manual provides more in-depth descriptions of common tasks and how to do them in Olex2.
  