
---
title: "Documentation"
linkTitle: "Documentation"
weight: 20
menu:
  olex2:
    weight: 10
description: Searchable and Structured Olex2 Documentation
---

{{< tag_cloud >}}

Writing meaningful documentation for a program as complex as Olex2 is very hard. Right from the start of Olex2 in 2004, we always dreamed about (and tried hard!) to produce crystallographic software that is so intuitive, that *no documentation* would be required. Well, *some* documentation will always be required, and here's our attempt at it.

## Search for it

Simply use the **Search Box** to look for content, or follow the **navigation tree** on the left - the choice is yours!

### Documentation in other Languages

Right now, all of our own documentation is in English. Over the years, many of our users have contributed a wealth of material in many languages. 

As we further update this Site, Documentation in different languages is best found by searching the Web for them.

### Olex2: A living project!

Since Olex2 is under constant development, it is a real challenge to keep up with a meaningful documentation of a large program like Olex2.

This is our attempt at putting everything into one place.

The documentation effort you are looking at right now is generated via GitHub -- and we invite you to contribute to it.

Please get in touch if you would like to help us with it!
